//
// Simple JSON I/O
//
// Author: Christian Holm Christensen
//
// Distributed under the LGPL 2 or any later version
//
#include <TJSON.h>
#include <sstream>
#include <cassert>
#include <cmath>
#include <TArrayI.h>
#include <TArrayD.h>
#include <TString.h>

namespace {
  /** Helper to turn character into string */
  std::string CtoS(char c)
  {
    std::stringstream s;
    s << c;
    return s.str();
  }
  /** 
   * Escape special characters in a string 
   *
   * @param str String to escape in 
   *
   * @return String with special characters escaped 
   */
  std::string Escape(const std::string &str)
  {
    std::string output;
    for( unsigned i = 0; i < str.length(); ++i )
      switch (str[i]) {
      case '\"': output += "\\\""; break;
      case '\\': output += "\\\\"; break;
      case '\b': output += "\\b";  break;
      case '\f': output += "\\f";  break;
      case '\n': output += "\\n";  break;
      case '\r': output += "\\r";  break;
      case '\t': output += "\\t";  break;
      default  : output += str[i]; break;
      }
    return output;
  }
}

//--------------------------------------------------------------------
TJSON::TJSON(std::initializer_list<TJSON> list)
  : TJSON()
{
  SetType(Class::OBJECT);
  for (auto i = list.begin(); i != list.end(); i+=2) {
    operator[](i->ToString()) = *(i+1); 
  }
}

//--------------------------------------------------------------------
TJSON::TJSON(TJSON&& other)
  : fInternal(other.fInternal),
    fType(other.fType)
{
  other.fType          = Class::NONE;
  other.fInternal.fMap = nullptr;
}

//--------------------------------------------------------------------
TJSON::TJSON(const TJSON& other)
  : TJSON()
{
  switch(other.fType) {
  case Class::OBJECT:
    fInternal.fMap = new Mapping(other.fInternal.fMap->begin(),
				 other.fInternal.fMap->end());
    break;
  case Class::ARRAY:
    fInternal.fList = new List(other.fInternal.fList->begin(),
			       other.fInternal.fList->end());
    break;
  case Class::STRING:
    fInternal.fString = new std::string(*other.fInternal.fString);
    break;
  default:
    fInternal = other.fInternal;
  }
  fType = other.fType;
}

//--------------------------------------------------------------------
TJSON& TJSON::operator=(TJSON&& other)
{
  fInternal.Clear(fType);
  fInternal            = other.fInternal;
  fType                = other.fType;
  other.fInternal.fMap = nullptr;
  other.fType          = Class::NONE;
  return *this;
}

//--------------------------------------------------------------------
TJSON& TJSON::operator=(const TJSON &other)
{
  fInternal.Clear(fType);
  switch(other.fType) {
  case Class::OBJECT:
    fInternal.fMap = 
      new Mapping(other.fInternal.fMap->begin(),
		  other.fInternal.fMap->end());
    break;
  case Class::ARRAY:
    fInternal.fList = 
      new List(other.fInternal.fList->begin(),
	       other.fInternal.fList->end());
    break;
  case Class::STRING:
    fInternal.fString = new std::string(*other.fInternal.fString);
    break;
  default:
    fInternal = other.fInternal;
  }
  fType = other.fType;
  return *this;
}

//--------------------------------------------------------------------
TJSON::~TJSON()
{
  fInternal.Clear(fType);
}

//--------------------------------------------------------------------
TJSON::TJSON(const TArrayD& c)
  : TJSON()
{
  SetType(Class::ARRAY,c.GetSize());
  for (int i = 0; i < c.GetSize(); i++)
    this->operator[](i) = c[i];
}
//--------------------------------------------------------------------
TJSON::TJSON(const TArrayI& c)
  : TJSON()
{
  SetType(Class::ARRAY,c.GetSize());
  for (int i = 0; i < c.GetSize(); i++)
    this->operator[](i) = c[i];
}
//--------------------------------------------------------------------
TJSON::TJSON(const TString& s)
  : fInternal(std::string(s.Data())), fType(Class::STRING)
{}
  
//--------------------------------------------------------------------
int TJSON::Length() const
{
  return fType == Class::ARRAY ? fInternal.fList->size() : -1;
}
//--------------------------------------------------------------------
bool TJSON::HasKey(const std::string& k) const
{
  if (fType != Class::OBJECT)
    return false;
  return
    fType == Class::OBJECT ? 
    fInternal.fMap->find(k) != fInternal.fMap->end() :
    false;
}
//--------------------------------------------------------------------
int TJSON::Size() const
{
  return
    fType == Class::OBJECT ?  fInternal.fMap->size() :
    fType == Class::ARRAY  ?  fInternal.fList->size() : 
    -1;
}

//--------------------------------------------------------------------
TJSON& TJSON::operator[](const std::string& key)
{
  SetType(Class::OBJECT);
  return fInternal.fMap->operator[](key);
}
//--------------------------------------------------------------------
const TJSON& TJSON::operator[](const std::string& key) const
{
  if (!HasKey(key)) return Null();
  return fInternal.fMap->operator[](key);
}
//--------------------------------------------------------------------
TJSON& TJSON::operator[](unsigned index)
{
  SetType(Class::ARRAY);
  if (index >= fInternal.fList->size())
    fInternal.fList->resize(index + 1);
  return fInternal.fList->operator[](index);
}

//--------------------------------------------------------------------
const TJSON& TJSON::operator[](unsigned index) const
{
  if (index >= fInternal.fList->size()) Null();
  return fInternal.fList->operator[](index);
}

//--------------------------------------------------------------------
const TJSON &TJSON::At(const std::string& key) const 
{
  return fInternal.fMap->at(key);
}

//--------------------------------------------------------------------
const TJSON& TJSON::At(unsigned index) const
{
  return fInternal.fList->at(index);
}

//--------------------------------------------------------------------
std::string TJSON::ToString() const
{
  bool b;
  return std::move(ToString(b));
}

//--------------------------------------------------------------------
std::string TJSON::ToString(bool &ok) const
{
  ok = (fType == Class::STRING);
  return ok ? std::move(Escape(*fInternal.fString)): std::string("");
}

//--------------------------------------------------------------------
double TJSON::ToFloat() const
{
  bool b;
  return ToFloat(b);
}

//--------------------------------------------------------------------
double TJSON::ToFloat( bool &ok ) const
{
  ok = (fType == Class::FLOATING);
  return ok ? fInternal.fFloat : 0.0;
}

//--------------------------------------------------------------------
long TJSON::ToInt() const
{
  bool b;
  return ToInt(b);
}

//--------------------------------------------------------------------
long TJSON::ToInt(bool &ok) const
{
  ok = (fType == Class::INTEGRAL);
  return ok ? fInternal.fInt : 0;
}

//--------------------------------------------------------------------
bool TJSON::ToBool() const
{
  bool b;
  return ToBool( b );
}

//--------------------------------------------------------------------
bool TJSON::ToBool(bool &ok) const
{
  ok = (fType == Class::BOOLEAN);
  return ok ? fInternal.fBool : false;
}

//--------------------------------------------------------------------
void TJSON::ToArray(TArrayI& c)
{
  c.Set(Length());
  if (Length() <= 0) return;

  size_t j = 0;
  for (auto& i : ArrayRange())
    c[j++] = i.ToInt();
}

//--------------------------------------------------------------------
void TJSON::ToArray(TArrayD& c)
{
  c.Set(Length());
  if (Length() <= 0) return;

  size_t j = 0;
  for (auto& i : ArrayRange())
    c[j++] = i.ToInt();
}
  
//--------------------------------------------------------------------
TJSON::Wrapper<TJSON::Mapping> TJSON::ObjectRange()
{
  if (fType == Class::OBJECT)
    return Wrapper<Mapping>(fInternal.fMap);
  return Wrapper<Mapping>(nullptr);
}
  
//--------------------------------------------------------------------
TJSON::ConstWrapper<TJSON::Mapping> TJSON::ObjectRange() const
{
  if (fType == Class::OBJECT)
    return ConstWrapper<Mapping>(fInternal.fMap);
  return ConstWrapper<Mapping>(nullptr);
}

//--------------------------------------------------------------------
TJSON::Wrapper<TJSON::List> TJSON::ArrayRange()
{
  if (fType == Class::ARRAY)
    return Wrapper<List>(fInternal.fList);
  return Wrapper<List>(nullptr);
}

//--------------------------------------------------------------------
TJSON::ConstWrapper<TJSON::List> TJSON::ArrayRange() const
{
  if (fType == Class::ARRAY)
    return ConstWrapper<List>(fInternal.fList);
  return ConstWrapper<List>(nullptr);
}

//--------------------------------------------------------------------
std::istream& TJSON::Read(std::istream& in)
{
  ReadWs(in);
  if (in.eof()) return in;

  char value = in.peek();
  switch (value) {
  case '[' : return ReadArray (in);
  case '{' : return ReadObject(in);
  case '\"': return ReadString(in);
  case 't' :
  case 'f' : return ReadBool(in);
  case 'n' : return ReadNull(in);
  default  : if ((value <= '9' && value >= '0')
		 || value == 'N'
		 || value == 'I'
		 || value == '-')
      return ReadNumber(in);
  }
  throw std::runtime_error("Unknown starting character: '"
			   +CtoS(value)+"' ("
			   +std::to_string(int(value))+")");;
  return in;
}

//--------------------------------------------------------------------
std::ostream& TJSON::Write(std::ostream& out,
			   const std::string& cur,
			   const std::string& tab) const
{
  switch(fType) {
  case Class::BOOLEAN:  return WriteBool(out);
  case Class::INTEGRAL: return WriteInt(out);
  case Class::FLOATING: return WriteFloat(out);
  case Class::STRING:   return WriteString(out);
  case Class::ARRAY:    return WriteArray(out, cur,tab);
  case Class::OBJECT:   return WriteObject(out, cur,tab);
  case Class::NONE:
    break;
  }
  return out << "null";
}

//--------------------------------------------------------------------
TJSON TJSON::Load(std::istream& in)
{
  TJSON j;
  j.Read(in);
  return j;
}
//--------------------------------------------------------------------
TJSON TJSON::Load(const std::string& in)
{
  std::stringstream s(in);
  return Load(s);
}
  
//--------------------------------------------------------------------
void TJSON::Dump(std::ostream& o, const std::string& tab)
{
  Write(o,"",tab);
}

//--------------------------------------------------------------------
std::string TJSON::Dump(const std::string& tab)
{
  std::stringstream s;
  Dump(s,tab);
  return s.str();
}

//--------------------------------------------------------------------
void TJSON::SetType(Class type,size_t n)
{
  if (type == fType) return;
  
  fInternal.Clear(fType);
  
  switch (type) {
  case Class::NONE:      fInternal.fMap    = nullptr;            break;
  case Class::OBJECT:    fInternal.fMap    = new Mapping();      break;
  case Class::ARRAY:     fInternal.fList   = new List(n);        break;
  case Class::STRING:    fInternal.fString = new std::string();  break;
  case Class::FLOATING:  fInternal.fFloat  = 0.0;                break;
  case Class::INTEGRAL:  fInternal.fInt    = 0;                  break;
  case Class::BOOLEAN:   fInternal.fBool   = false;              break;
  }

  fType = type;
}

//--------------------------------------------------------------------
std::ostream& TJSON::WriteBool(std::ostream& out) const
{
  return out << (fInternal.fBool ? "true" : "false");
}
//--------------------------------------------------------------------
std::ostream& TJSON::WriteInt(std::ostream& out) const
{
  return out << fInternal.fInt;
}
//--------------------------------------------------------------------
std::ostream& TJSON::WriteFloat(std::ostream& out) const
{
  if (std::isnan(fInternal.fFloat) or
      std::isnan(-fInternal.fFloat))
    return out << (kNonFiniteIsNull ? "null" : "NaN");
  if (std::isinf(fInternal.fFloat)) {
    if (kNonFiniteIsNull)
      return out << "null";
    return out << (fInternal.fFloat < 0 ? "-" : "")
	       << "Infinity";
  }
  return out << fInternal.fFloat;
}
//--------------------------------------------------------------------
std::ostream& TJSON::WriteString(std::ostream& out) const
{
  return out << "\"" + Escape(*fInternal.fString) + "\"";
}
//--------------------------------------------------------------------
std::ostream& TJSON::WriteArray(std::ostream& out,
				const std::string& cur,
				const std::string& tab) const
{
  bool sep = false;
  out << '[';
  for(auto& p : *fInternal.fList) {
    if (sep) out << ", ";
    sep = true;
    p.Write(out,cur+tab,tab);
  }
  return out << ']';
}
//--------------------------------------------------------------------
std::ostream& TJSON::WriteObject(std::ostream& out,
				 const std::string& cur,
				 const std::string& tab) const
{
  bool sep = false;
  out << "{";
  for(auto& p : *fInternal.fMap) {
    if (sep) out << ",";
    sep = true;
    out << "\n" << cur+tab << '\"' << p.first << "\": ";
    p.second.Write(out,cur+tab,tab);
  }
  return out << '\n' << cur << "}";
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadWs(std::istream& in)
{
  while (std::isspace(in.peek())) in.get();
  return in;
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadArray(std::istream& in)
{
  assert(in.get() == '[');
  SetType(Class::ARRAY);
  do {
    ReadWs(in);
    char n = in.peek();
    if (n == ']') {
      in.get();
      break;
    }

    ReadWs(in);
    n = in.peek();
    if (n == ',') in.get();
	
    TJSON e;
    e.Read(in);
    Append(e);
  } while(true);
  return in;
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadObject(std::istream& in)
{
  assert(in.get() == '{');
  SetType(Class::OBJECT);

  do {
    ReadWs(in);
    char n = in.peek();
    if (n == '}') {
      in.get();
      break;
    }

    ReadWs(in);
    n = in.peek();
    if (n == ',') in.get();

    TJSON k;
    k.Read(in);
    assert (k.Type() == Class::STRING);

    ReadWs(in);
    assert(in.get() == ':');

    TJSON v;
    v.Read(in);
    operator[](k.ToString()) = v;
  } while (true);
  return in;
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadString(std::istream& in)
{
  assert(in.get() == '"');

  std::string v;
  do {
    char n = in.peek();
    if (n == '\"') {
      in.get();
      break;
    }

    n = in.get();
    if (n == '\\') {
      n = in.get();
      switch (n) {
      case '\"': v += '\"'; break;
      case '\\': v += '\\'; break;
      case '/' : v += '/' ; break;
      case 'b' : v += '\b'; break;
      case 'f' : v += '\f'; break;
      case 'n' : v += '\n'; break;
      case 'r' : v += '\r'; break;
      case 't' : v += '\t'; break;
      case 'u' : 
	/* Unicode */
	v += "\\u" ;
	for (unsigned i = 1; i <= 4; ++i) {
	  n = in.get();
	  if ((n >= '0' && n <= '9') ||
	      (n >= 'a' && n <= 'f') ||
	      (n >= 'A' && n <= 'F'))
	    v += n;
	  else
	    throw std::runtime_error("Invalid unicode digit "+
				     std::string(1,n));
	}
	break;
      default: v += '\\'; break;
      }
    }
    else
      v += n;
  } while (true);

  this->operator=(v);
  return in;
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadBool(std::istream& in)
{
  std::string s;
  for (size_t i = 0; i < 4; i++) s += in.get();

  if (s == "true") {
    this->operator=(true);
    return in;
  }

  s += in.get();
  if (s == "false") {
    this->operator=(false);
    return in;
  }
      
  throw std::runtime_error("Invalid boolean value: "+s);
  return in;
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadNull(std::istream& in)
{
  std::string s;
  for (size_t i = 0; i < 4; i++) s += in.get();

  if (s != "null")
    throw std::runtime_error("Invalid null value: "+s);

  SetType(Class::NONE);
  return in;
}
//--------------------------------------------------------------------
std::istream& TJSON::ReadNumber(std::istream& in)
{
  char p = in.peek();
  if (p == 'N') {
    std::string s = {char(in.get()), char(in.get()), char(in.get())};
    if (s == "NaN" and !kNonFiniteIsNull) {
      this->operator=(NAN);
      return in;
    }

    throw std::runtime_error("Invalid number value: "+s);
  }

  double sign = 1;
  if (p == '-') {
    sign = -1;
    in.get();
  }

  p = in.peek();
  if (p == 'I') {
    // Infinity is 8 characters 
    std::string s = {char(in.get()), char(in.get()),
      char(in.get()), char(in.get()),
      char(in.get()), char(in.get()),
      char(in.get()), char(in.get())};
    if (s == "Infinity" and !kNonFiniteIsNull) {
      this->operator=(sign*INFINITY);
      return in;
    }

    throw std::runtime_error("Invalid number value: "+s);
  }
	
	  
  double v;
  in >> v;

  if (std::nearbyint(v) == v) 
    this->operator=(long(sign*v));
  else 
    this->operator=(sign*v);

  return in;
}

//--------------------------------------------------------------------
const TJSON& TJSON::Null()
{
  static TJSON* fgNull = 0;
  if (!fgNull) fgNull = new TJSON();
  return *fgNull;
}

//--------------------------------------------------------------------
TJSON TJSON::Array()
{
  return std::move(TJSON::Make(TJSON::Class::ARRAY,0));
}
//--------------------------------------------------------------------
TJSON TJSON::Array(size_t n)
{
  return std::move(TJSON::Make(TJSON::Class::ARRAY,n));
}
//--------------------------------------------------------------------
TJSON TJSON::Object()
{
  return std::move(TJSON::Make(TJSON::Class::OBJECT));
}

void TJSON::Print(Option_t* opt) const
{
  Write(std::cout, "", opt);
}

const char* TJSON::GetName() const
{
  switch(fType) {
  case Class::BOOLEAN:  return "BOOL";
  case Class::INTEGRAL: return "INT";
  case Class::FLOATING: return "FLOAT";
  case Class::STRING:   return "STRING";
  case Class::ARRAY:    return "ARRAY";
  case Class::OBJECT:   return "OBJECT";
  case Class::NONE:;
  }
  return "NONE";
}
const char* TJSON::GetTitle() const
{
  switch(fType) {
  case Class::BOOLEAN:  return Form("%s", ToBool() ? "true" : "false");
  case Class::INTEGRAL: return Form("%ld", ToInt());
  case Class::FLOATING: return Form("%g", ToFloat());
  case Class::STRING:   return Form("\"%s\"",ToString().c_str());
  case Class::ARRAY:    return "[...]";
  case Class::OBJECT:   return "{...}";
  case Class::NONE:;
  }
  return "NONE";
}
//--------------------------------------------------------------------
std::ostream& operator<<(std::ostream &out, const TJSON &json)
{
  std::string tab;
  for (size_t i = 0; i < json.kDefaultTab; i++) tab += " ";
  
  return json.Write(out,"",tab);
}

//--------------------------------------------------------------------
std::istream& operator>>(std::istream &in, TJSON &json)
{
  return json.Read(in);;
}

//
// EOF
//

# simplejson

A simple C++ JSON library

## Installation 

The library lives in the header file [`json.hh`](json.hh) which can be
put anywhere you like in your include path. 

## Examples, tests, and documentation

Run 

	make 
	
to make 

- `examples` (from [`examples.cc`](examples.cc)) which shows some
  simple examples 
  
- `test` (from [`test.cc`](test.cc)) which reads in JSON either from
  file or standard input and writes it back to standard output 
  
- `html` which contains API documentation as HTML 

## Testing 

Run 

	./test.py 
	
to run some tests using Python's JSON library for validation 

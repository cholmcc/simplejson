#!/usr/bin/env python3
tests = [
    '''1.20E+2''',  # test13
    '''null''',  # test1
    '''true''',  # test2
    '''100''',  # test3
    '''1.234''',  # test4
    '''"StringTest"''',  # test5
    '''{}''',  # test6
    '''{
        "Key" : "Value"
    }''',  # test7
    '''[]''',  # test8
    '''[1,2,3]''',  # test9
    r'''"This is a\n\nMultiline string"''',  # test10
    r'''{
        "T1" : "Value With a Quote : \"",
        "T2" : "Value With a Rev Solidus : \/",
        "T3" : "Value with a Solidus : \\",
        "T4" : "Value with a Backspace : \b",
        "T5" : "Value with a Formfeed : \f",
        "T6" : "Value with a Newline : \n",
        "T7" : "Value with a Carriage Return : \r",
        "T8" : "Value with a Horizontal Tab : \t"
    }''',  # test11
    '''""''',  # test12
]

from subprocess import Popen, PIPE
import json
import os

for n,t in enumerate(tests):
    fn = f'test{n:02d}.json'
        
    try:
        print(f'Test # {n}: ',end='')
        
        with open(fn,'w') as file:
            file.write(t)

        with Popen(['./test',fn],stdout=PIPE) as p:
            o = p.stdout.read()

        expected = json.loads(t)
        actual   = json.loads(o)

        if not expected == actual:
            print(f'Failed!\n'
                  f'Expected: {expected}'
                  f'Got:      {actual}',
                  out=os.stderr)
        
        else:
            print('Passed')

    except Exception as e:
        print(f'Error, subprocesss {n} failed: {e}')
        print(f'Input:\n{t}')
        print(f'Output:\n{o}')

    finally:
        pass
    # os.unlink(fn)
        


        

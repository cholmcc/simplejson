#
#
#
CXX		= g++ -c
CXXFLAGS	= -std=c++11 -g -Wall -O3
CPPFLAGS	= -I.
LD		= g++
LDFLAGS		=

EXAMPLES	= examples
TESTS		= test

%.hh.gch:%.hh
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $<

%.o:%.cc
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $<

%:%.o
	$(LD) $(LDFLAGS) $< -o $@

%.o:%.cxx
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $<

%_dict.cxx:%.h
	rootcling -I. -rml lib$*.so -rmf lib$*.rootmap $@ $^ 


all:	$(EXAMPLES) $(TESTS) doc

libTJSON.so:	TJSON.o TJSON_dict.o
	$(LD) -shared -o $@ $^

TJSON.o:	CPPFLAGS=-I. $(shell root-config --cflags)
TJSON.o:	CXXFLAGS=-fPIC -std=c++11 -g -Wall
TJSON_dict.o:	CPPFLAGS=-I. $(shell root-config --cflags)
TJSON_dict.o:	CXXFLAGS=-fPIC -std=c++11 -g -Wall

clean:
	rm -f *~ *.gch *.o $(EXAMPLES) $(TESTS) *.json
	rm -f *.rootmap *.so *_dict.*
	rm -rf html 

doc:	Doxyfile json.hh examples.cc
	doxygen -s $< 

examples.o:examples.cc json.hh.gch
test.o:test.cc json.hh.gch


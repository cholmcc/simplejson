#include <json.hh>
#include <fstream>

int main(int argc, char** argv)
{
  std::istream* pin = &std::cin;
  if (argc > 1)
    pin = new std::ifstream(argv[1]);

  std::istream& in = *pin;
  
  json::JSON j;
  in >> j;

  std::cout << j << std::endl;
  return 0;
}

  
  

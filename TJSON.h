// -*- mode: C++ -*-
//
// Simple JSON I/O
//
// Author: Christian Holm Christensen
//
// Distributed under the LGPL 2 or any later version
//
#ifndef ROOT_TJSON
#define ROOT_TJSON
#include <TObject.h>
#include <TParameter.h>
#include <string>
#include <deque>
#include <map>
#include <initializer_list>
#include <iostream>

class TArrayD;
class TArrayI;
class TString;

namespace
{
  /** 
   * @{ 
   * @name Short-hands 
   */
  template <typename T>  using is_bool  = std::is_same<T,bool>;
  template <typename T>  using is_float = std::is_floating_point<T>;
  template <typename T>  using is_string= std::is_convertible<T,std::string>;
  template <typename T>  using is_int   = std::is_integral<T>;
  template <typename...> using void_tt  = void;
  /* @} */
  /** 
   * @{ 
   * @name other type deduction 
   */
  /** 
   * Detect if a type is iterable 
   */
  template <typename T, typename = void>
  struct is_iterable : std::false_type {};

  /**
   * Detect if a type is iterable 
   */
  template <typename T>
  struct is_iterable<T, void_tt<decltype(std::begin(std::declval<T>())),
				decltype(std::end(std::declval<T>()))>>
    : std::true_type {};

  /** Detect if a type is an iterator */
  template<typename T, typename = void>
  struct is_iterator : std::false_type {};

  template<typename T>
  struct is_iterator<T,
		     typename std::enable_if<!std::is_same<typename
							   std::iterator_traits<T>::value_type, void>::value>::type> : std::true_type {};
  /** 
   * Detect if type has @c key_type sub-type 
   */
  template<typename, typename = void_tt<> >
  struct has_key_type : std::false_type { };
 
  /** 
   * Detect if type has @c key_type sub-type 
   */
  template<typename T>
  struct has_key_type<T, void_tt<typename T::key_type>>
    : std::true_type { };
  /* @} */
}

/**
 * A simple JSON input/output class (not TObject serialisation).
 *
 * Heavily inspired by https://github.com/nbsdx/SimpleJSON.git 
 *
 * @section basics A basic example of usage.  
 *
 * Here we create an object of each possible kind.
 *
 * @dontinclude Example.C 
 * @skip basic()
 * @until TJSON::Object
 *
 * The type of an object is not fixed.  By assigning a different
 * value the type is changed.
 *
 * @until b = std::string
 *
 * We can easily append elements to an array 
 *
 * @until a.append(false)
 *
 * Elements of an array are accessed by index 
 *
 * @until a[0]
 *
 * Initializing an array directly requires using TJSON::Array 
 *
 * @until TJSON::Array
 *
 * Object properties are assigned by string keys 
 *
 * @until a2
 *
 * Objects can be nested within other objects 
 *
 * @until o2
 *
 * Finally, we can print out any JSON object 
 *
 * @until } 
 *
 * @section to Extracting value 
 *
 * We can use member functions to extract values of basic types (@c
 * bool, @c string, @c float, and @c int)
 *
 * @until }
 *
 * @section init Fancy initialisation 
 *
 * We can initialize and object using _almost_ JSON syntax.  Note,
 * however, that we must use @c , as key-value separator in objects,
 * and arrays must explicity use TJSON::Array.
 *
 * Alternatively, one can initialize with an explicit std::map.
 *
 * @until // end_init 
 *
 * @section iter Iteration over arrays and objects 
 *
 * We can iterate over array elements via TJSON::ArrayRange and
 * objects via TJSON::ObjectRange.
 *
 * @until }
 *
 * @section array Array assignment 
 *
 * Assigning to an object using index keys turns the object into an
 * array.  The array is padded up to the assigned index (but not
 * truncated).  Sub-indexes are also padded as necessary (with `null`
 * values).
 *
 * @until } 
 *
 * @section load Loading from string or stream
 *
 * JSON objects can be loaded from a string or stream. 
 *
 * @until } // end_load
 *
 * @author Christian Holm Christensen <cholm@nbi.dk>
 */
class TJSON : public TObject
{
public:
  /** The type object */
  enum class Class {
    NONE,
    OBJECT,
    ARRAY,
    STRING,
    FLOATING,
    INTEGRAL,
    BOOLEAN
  };
  /** Maps */
  using Mapping=std::map<std::string,TJSON>;
  /** Arrays */
  using List=std::deque<TJSON>;

  size_t kDefaultTab = 2;
  bool   kNonFiniteIsNull = false;

private:
  union Store {
    Store(double d) : fFloat(d) {}
    Store(long l) : fInt(l) {}
    Store(bool b) : fBool(b) {}
    Store(const std::string& s) : fString(new std::string(s)) {}
    Store() : fInt(0) {}

    /** When an array */
    List* fList;
    /** When a map */
    Mapping* fMap;
    /** When a string */
    std::string* fString;
    /** When a double */
    double fFloat;
    /** When an integer */
    long fInt;
    /** When a boolean */
    bool fBool;

    /** Reset data 

	Beware: only call if YOU know that _internal is allocated. No
	checks performed here.  This function should be called in a
	constructed JSON just before you are going to overwrite
	_internal...
    */
    void Clear(Class type)
    {
      switch (type) {
      case Class::OBJECT: delete fMap;    fMap    = nullptr; break;
      case Class::ARRAY:  delete fList;   fList   = nullptr; break;
      case Class::STRING: delete fString; fString = nullptr; break;
      default:;
      }
    }
  } fInternal;
  /** Type of the value */
  Class fType = Class::NONE;
    
public:
  /** A wrapper around a container */
  template <typename Container>
  class Wrapper
  {
  private:
    /** The container */
    Container* fObject;
  public:
    Wrapper(Container* o) : fObject(o) {}
    Wrapper(std::nullptr_t) : fObject(nullptr) {}
      
    /** Iterator type */
    using const_iterator = typename Container::const_iterator;
    /** Iterator type */
    using iterator = typename Container::iterator;

    /** Get iterator pointing to beginnning of container */
    iterator begin() { return fObject ? fObject->begin() : iterator(); }
    /** Get iterator pointing to just beyond end of container */
    iterator end() { return fObject ? fObject->end() : iterator(); }
    /** Get iterator pointing to beginnning of container */
    const_iterator begin()  const {
      return fObject ? fObject->begin() : iterator();
    }
    /** Get iterator pointing to just beyond end of container */
    const_iterator end() const {
      return fObject ? fObject->end() : iterator();
    }
  };
  /** A constant wrapper */
  /** A wrapper around a container */
  template <typename Container>
  class ConstWrapper
  {
  private:
    /** The container */
    const Container* fObject;
  public:
    ConstWrapper(Container* o) : fObject(o) {}
    ConstWrapper(std::nullptr_t) : fObject(nullptr) {}
      
    /** Iterator type */
    using const_iterator = typename Container::const_iterator;

    /** Get iterator pointing to beginnning of container */
    const_iterator begin()  const {
      return fObject ? fObject->begin() : const_iterator();
    }
    /** Get iterator pointing to just beyond end of container */
    const_iterator end() const {
      return fObject ? fObject->end() : const_iterator();
    }
  };
      
  /** Constructor - empty object */
  TJSON() : fInternal(), fType(Class::NONE) {}
  /** Constructor from initializer list */
  TJSON(std::initializer_list<TJSON> list);
  /** Move constructor */
  TJSON(TJSON&& other);
  /** Copy contructor */
  TJSON(const TJSON& other);
  /** Move assignment operator */
  TJSON& operator=(TJSON&& other);
  /** assignment operator */
  TJSON& operator=(const TJSON& other);
  /** Destructor */
  virtual ~TJSON();

  TJSON(const TArrayI& c);
  TJSON(const TArrayD& c);
  TJSON(const TString& s);

  template <typename T>
  TJSON(const TParameter<T>& p)
    : TJSON()
  {
    SetType(Class::OBJECT);
    this->operator[](p.GetName()) = p.GetVal();
  }
  /** 
   * SFINAE constructor from boolean type 
   * @param b Boolean value
   */
  template <typename T,
	    typename std::enable_if<is_bool<T>::value,int>::type = 0>
  TJSON(T b)
    : fInternal(b), fType(Class::BOOLEAN)
  {}

  /** 
   * SFINAE constructor from integer type 
   *
   * @param i Integer value 
   */
  template <typename T,
	    typename std::enable_if<is_int<T>::value &&
				    !is_bool<T>::value,int>::type = 0>
  TJSON(T i)
    : fInternal(long(i)), fType(Class::INTEGRAL)
  {}
    
  /** 
   * SFINAE constructor from float type 
   *
   * @param f Floating point value
   */
  template <typename T,
	    typename std::enable_if<is_float<T>::value,int>::type = 0>
  TJSON(T f)
    : fInternal(double(f)), fType(Class::FLOATING)
  {}

  /** 
   * SFINAE constructor from string type 
   *
   * @param s String value 
   */
  template <typename T,
	    typename std::enable_if<is_string<T>::value,int>::type = 0>
  TJSON(T s)
    : fInternal(std::string(s)), fType(Class::STRING){}

  /** 
   * SFINAE constructor from iterable
   *
   * @param c An iterable 
   */
  template <typename T,
	    typename std::enable_if<is_iterable<T>::value &&
				    !has_key_type<T>::value &&
				    !is_string<T>::value,int>::type = 0>
  TJSON(T c)
    : fInternal(), fType(Class::NONE)
  {
    auto b = std::begin(c);
    auto e = std::end(c);
    SetType(Class::ARRAY,std::distance(b,e));
    for (auto i = b; i != e; ++i)
      this->operator[](std::distance(b,i)) = *i;
  }
    
  /** SFINAE assignment operator from map iterable */
  template <typename T,
	    typename std::enable_if<is_iterable<T>::value &&
				    has_key_type<T>::value &&
				    is_string<typename T::key_type>::value &&
				    !is_string<T>::value,int>::type = 0>
  TJSON(T m)
    : fInternal(), fType(Class::NONE)
  {
    SetType(Class::OBJECT);

    for (auto& kv : m) 
      this->operator[](kv.first) = kv.second;
  }
  /** 
   * Constructor from null 
   */
  TJSON(std::nullptr_t) : fInternal(), fType(Class::NONE){}

  /** Make object of specified type */
  static TJSON Make(Class type, size_t n=0)
  {
    TJSON ret;
    ret.SetType(type, n);
    return ret;
  }
  /** 
   * @{ 
   * @name Queries 
   */
  /** 
   * Get length of object (if array), -1 otherwise 
   *
   * @returns number of elements 
   */
  int Length() const;
  /** 
   * Check if object has a key (if an object) 
   *
   * @param key Key to check for 
   *
   * @return true if object contains key 
   */
  bool HasKey(const std::string& key) const;

  /** 
   * Get size (in elements) of this if an array or object 
   *
   * @return Number of elements or values, or -1
   */
  int Size() const;

  /** 
   * Get the type 
   *
   * @param type specifier 
   */
  Class Type() const { return fType; }
  /* @} */

  /** 
   * @{ 
   * @name Assigning values 
   */
  /** Append an object */
  template <typename T>
  TJSON& Append(T arg)
  {
    SetType(Class::ARRAY);
    fInternal.fList->emplace_back(arg);
    return *this;
  }

  /** Append objects - variadic, using tail recursion to set elements */
  template <typename T, typename... U>
  TJSON& Append(T arg, U... args)
  {
    Append(arg);
    return Append(args...);
  }

  /** SFINAE assignment operator from boolean */
  template <typename T,
	    typename std::enable_if<is_bool<T>::value,int>::type = 0>
  TJSON& operator=(T b)
  {
    SetType(Class::BOOLEAN);
    fInternal.fBool = b;
    return *this;
  }

  /** SFINAE assignment operator from integer */
  template <typename T,
	    typename std::enable_if<is_int<T>::value &&
				    !is_bool<T>::value,int>::type = 0>
  TJSON& operator=(T i)
  {
    SetType(Class::INTEGRAL);
    fInternal.fInt = i;
    return *this;
  }

  /** SFINAE assignment operator from float */
  template <typename T,
	    typename std::enable_if<is_float<T>::value,int>::type = 0>
  TJSON& operator=(T f)
  {
    SetType(Class::FLOATING);
    fInternal.fFloat = f;
    return *this;
  }

  /** SFINAE assignment operator from string */
  template <typename T,
	    typename std::enable_if<is_string<T>::value,int>::type = 0>
  TJSON& operator=(T s)
  {
    SetType(Class::STRING);
    *fInternal.fString = std::string(s);
    return *this;
  }

  /** SFINAE assignment operator from iterable */
  template <typename T,
	    typename std::enable_if<is_iterable<T>::value &&
				    !has_key_type<T>::value &&
				    !is_string<T>::value,int>::type = 0>
  TJSON& operator=(T c)
  {
    auto b = std::begin(c);
    auto e = std::end(c);
      
    SetType(Class::ARRAY,std::distance(b,e));
	
    for (auto i = b; i != e; ++i)
      this->operator[](std::distance(b,i)) = *i;
    return *this;
  }

  /** SFINAE assignment operator from map iterable */
  template <typename T,
	    typename std::enable_if<is_iterable<T>::value &&
				    has_key_type<T>::value &&
				    is_string<typename T::key_type>::value &&
				    !is_string<T>::value,int>::type = 0>
  TJSON& operator=(T m)
  {
    SetType(Class::OBJECT);

    for (auto& kv : m) 
      this->operator[](kv.first) = kv.second;

    return *this;
  }
  /* @} */

  /** 
   * @{
   * @name Accessing value 
   */
  /** 
   * Value access with key
   *
   * @param key Key 
   *
   * @returns Value 
   */
  TJSON& operator[](const std::string& key);
  /** 
   * Value access with key
   *
   * @param key Key 
   *
   * @returns Value 
   */
  const TJSON& operator[](const std::string& key) const;
  /** 
   * Element access with integer index 
   *
   * @param index Index 
   *
   * @returns Element 
   */
  TJSON& operator[](unsigned index);
  /** 
   * Element access with integer index 
   *
   * @param index Index 
   *
   * @returns Element 
   */
  const TJSON& operator[](unsigned index) const;
  /** 
   * Value access with key
   *
   * @param key Key 
   *
   * @returns Value 
   */
  TJSON& At(const std::string& key) { return operator[](key); }
  /** 
   * Value access with key
   *
   * @param key Key 
   *
   * @returns Value 
   */
  const TJSON &At(const std::string& key) const;
  /** 
   * Element access with integer index 
   *
   * @param index Index 
   *
   * @returns Element 
   */
  TJSON& At(unsigned index) { return operator[](index); }
  /** 
   * Element access with integer index 
   *
   * @param index Index 
   *
   * @returns Element 
   */
  const TJSON& At(unsigned index) const;

  /**
   * Check if this is a null object
   */
  bool IsNull() const { return fType == Class::NONE; }
  /** 
   * Access as a string 
   *
   * @returns requested value 
   */
  std::string ToString() const;
  /** 
   * Access as string, with check 
   *
   * @param ok true on return if access is good 
   * 
   * @return as requested type
   */
  std::string ToString( bool &ok ) const;
  /** 
   * Access as a float 
   *
   * @returns requested value 
   */
  double ToFloat() const;
  /** 
   * Access as floating point, with check 
   *
   * @param ok true on return if access is good 
   * 
   * @return as requested type
   */
  double ToFloat( bool &ok ) const;
  /** 
   * Access as integer 
   *
   * @returns requested value 
   */
  long ToInt() const;
  /** 
   * Access as integer, with check 
   *
   * @param ok true on return if access is good 
   * 
   * @return as requested type
   */
  long ToInt(bool &ok) const;
  /** 
   * Access as boolean
   *
   * @returns requested value 
   */
  bool ToBool() const;
  /** 
   * Access as boolean, with check 
   *
   * @param ok true on return if access is good 
   * 
   * @return as requested type
   */
  bool ToBool(bool &ok) const;

  void ToArray(TArrayI& c);
  void ToArray(TArrayD& c);
  /**
   * Extract values into an array 
   */
  template <typename C,
	    typename std::enable_if<is_iterable<C>::value &&
				    is_float<typename C::value_type>::value,
				    int>::type = 0>
  void ToArray(C& c) const
  {
    int n = Length();
    if (n <= 0) return;

    c.resize(n);
    size_t j = 0;
    for (auto& i : ArrayRange())
      c[j++] = i.ToFloat();
  }      
  /**
   * Extract values into an array 
   */
  template <typename C,
	    typename std::enable_if<is_iterable<C>::value &&
				    is_int<typename C::value_type>::value,
				    int>::type = 0>
  void ToArray(C& c) const
  {
    int n = Length();
    if (n <= 0) return;

    c.resize(n);
    size_t j = 0;
    for (auto& i : ArrayRange())
      c[j++] = i.ToInt();
  }      
  /**
   * Extract values into an array 
   */
  template <typename C,
	    typename std::enable_if<is_iterable<C>::value &&
				    is_string<typename C::value_type>::value,
				    int>::type = 0>
  void ToArray(C& c) const
  {
    int n = Length();
    if (n <= 0) return;

    c.resize(n);
    size_t j = 0;
    for (auto& i : ArrayRange())
      c[j++] = i.ToString();
  }      
  /* @} */
    
  /** 
   * @{ 
   * @name Iteration 
   */
  /** 
   * Get view of an object
   *
   * @returns a container wrapper that allows iteration over object
   * properties. .
   */
  Wrapper<Mapping> ObjectRange();
  /** 
   * Get view of an object
   *
   * @returns a container wrapper that allows iteration over object
   * properties. .
   */
  ConstWrapper<Mapping> ObjectRange() const;
  /** 
   * Get view of an array 
   *
   * @returns a container wrapper that allows iteration over array
   * elements.
   */
  Wrapper<List> ArrayRange();
  /** 
   * Get view of an array 
   *
   * @returns a container wrapper that allows iteration over array
   * elements.
   */
  ConstWrapper<List> ArrayRange() const;
  /* @} */

  /** 
   * Read object (and nested objects) from stream 
   *
   * @param in Input stream to read from 
   *
   * @return @a in 
   */
  std::istream& Read(std::istream& in);
  /** 
   * Write object (and nested objects) to stream 
   *
   * @param out Output stream 
   * @param cur Current indent 
   * @param tab Indention step
   *
   * @return @a out 
   */
  std::ostream& Write(std::ostream& out,
		      const std::string& cur="",
		      const std::string& tab="  ") const;
  /** 
   * Load an object from stream 
   *
   * @param in Stream to read from 
   *
   * @return Read JSON object. 
   */
  static TJSON Load(std::istream& in);
  /** 
   * Load an object from string
   *
   * @param str String to read from 
   *
   * @return Read JSON object. 
   */
  static TJSON Load(const std::string& str);
  /** 
   * Dump a TJSON object to stream 
   *
   * @param o   Stream to write to
   * @param tab Indention per level 
   */
  void Dump(std::ostream& o, const std::string& tab="  ");
  /** 
   * Dump a TJSON object to a string
   *
   * @param tab Indention per level 
   *
   * @return String of TJSON object
   */
  std::string Dump(const std::string& tab="  ");
  /* @} */

  /** 
   * Make an array 
   *
   * @return TJSON object
   */
  static TJSON Array();
  
  /** 
   * Make an array 
   *
   * @return TJSON object
   */
  static TJSON Array(size_t n);

  /** 
   * Make an array 
   *
   * @param args Arguments (variadic)
   *
   * @return TJSON object
   */
  template <typename... T>
  static TJSON Array(T... args)
  {
    TJSON arr = TJSON::Make(TJSON::Class::ARRAY);
    arr.Append(args...);
    return arr;
  }

  template <typename Iterator,
	    typename std::enable_if<is_iterator<Iterator>::value,int>::type = 0>
  static TJSON Array(Iterator begin, Iterator end)
  {
    TJSON arr = TJSON::Make(TJSON::Class::ARRAY, std::distance(begin,end));
    for (Iterator i = begin; i != end; ++i)
      arr[std::distance(begin,i)] = *i;

    return arr;
  }
  
  /** 
   * Make an object 
   *
   * @return TJSON object
   */
  static TJSON Object();

  void Print(Option_t* option="") const;
  const char* GetName() const;
  const char* GetTitle() const;
private:
  void SetType(Class type,size_t n=0);
  std::ostream& WriteBool(std::ostream& out) const;
  std::ostream& WriteInt(std::ostream& out) const;
  std::ostream& WriteFloat(std::ostream& out) const;
  std::ostream& WriteString(std::ostream& out) const;
  std::ostream& WriteArray(std::ostream& out,
			   const std::string& cur,
			   const std::string& tab) const;
  std::ostream& WriteObject(std::ostream& out,
			    const std::string& cur,
			    const std::string& tab) const;
  std::istream& ReadWs(std::istream& in);
  std::istream& ReadArray(std::istream& in);
  std::istream& ReadObject(std::istream& in);
  std::istream& ReadString(std::istream& in);
  std::istream& ReadBool(std::istream& in);
  std::istream& ReadNull(std::istream& in);
  std::istream& ReadNumber(std::istream& in);
  static const TJSON& Null();
  
};

std::ostream& operator<<(std::ostream &out, const TJSON &json);
std::istream& operator>>(std::istream &in, TJSON &json);

#endif




	
      
      
  


